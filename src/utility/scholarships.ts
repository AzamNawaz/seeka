import { Scholarship } from "src/Models/scholarschips-Interface";

export const scholarships: Scholarship[] = [
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship 1`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship 2`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship 3`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship 4`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship 5`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship 6`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship 7`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship 8`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship 9`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship 10`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship 11`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship 12`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship 13`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship 14`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship 15`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship 16`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship 17`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship 18`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship 19`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship 20`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University World Class Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Summer Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Postgraduate Research Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `Taylor's University Mechanical Engineering Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
    {
        image: `https://scontent.flhe21-1.fna.fbcdn.net/v/t1.18169-9/994132_433777676762851_4667999183155682713_n.png?_nc_cat=106&ccb=1-5&_nc_sid=85a577&_nc_ohc=5uOThW_ryXwAX8aXpkk&_nc_ht=scontent.flhe21-1.fna&oh=00_AT8vJVKodtrxjkNbz8dPd7b_5_I3xrizGzxKji1PR04CAA&oe=62138C98`,
        name: `The Gates Scholarship`,
        provider: `Taylor's University`,
        description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
        deadline: '18 March 2020',
        validity: 'Domestic & International',
        studyLevel: `Degree | Postgraduate`,
        intake: `May 2020`,
        saved: false,
        amount: {
            total: 2915,
            currency: 'USD',
            for: 'year'
        }
    },
]