export interface Scholarship {
    image: string,
    name: string,
    provider: string,
    description: string,
    deadline: string,
    validity: string,
    studyLevel: string,
    intake: string,
    saved:boolean,
    amount: {
        total: number,
        currency: string,
        for: string
    }
}