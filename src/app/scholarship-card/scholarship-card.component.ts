import { Component, Input, OnInit } from '@angular/core';
import { Scholarship } from 'src/Models/scholarschips-Interface';
import { scholarships } from 'src/utility/scholarships';

@Component({
  selector: 'app-scholarship-card',
  templateUrl: './scholarship-card.component.html',
  styleUrls: ['./scholarship-card.component.scss']
})
export class ScholarshipCardComponent implements OnInit {

  @Input() scholarship: Scholarship;

  constructor() {
    this.scholarship = scholarships[0];
  }

  ngOnInit(): void { }
  
  /**Add bookmark */
  bookmarkScholarship() {
    this.scholarship.saved = !this.scholarship.saved;
  }
}
