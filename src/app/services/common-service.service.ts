import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  pagination(currentPage: any, lastPage: any) {
    var delta = 2, /**Offset  to show number of pages before and after current page*/
      left = currentPage - delta,/**Starting point */
      right = currentPage + delta,/**Ending point */
      range = [],
      rangeWithDots: any[] = [],
      previousPage: any;

    /**Find actual range without ellipsis */

    range.push(1) /**pages will always start with 1 */

    for (let i = left; i <= right; i++) {
      /**Because we already pushing first and last page we have to check so that there is no duplication*/
      if (i > 1 && i < lastPage) {
        range.push(i);
      }
    }

    /**Always end with last page */
    range.push(lastPage);
    /**END */

    /**Now here we add ellipsis by comparing two consective pages and checking there difference if difference is 1 no need for ellipsis if difference is great than 1 and 2 only than add ellipsis if difference is equals to 2 that means there is only one page left so we add that page rather than to show ellipsis*/

    range.forEach(page => {
      /**Checking if there is previous existing otherwise push page and set previous page*/
      if (previousPage) {
        /**Once prvious page is set now here we check if difference is equals to 2 that means there is only one page and we add that page*/
        if (page - previousPage === 2) {

          rangeWithDots.push(previousPage + 1);

        } else if (page - previousPage !== 1) {
          /**if the difference is not equals to 1 and 2 here this means there more than 2 pages in between previous and current page so add ellipsis */
          rangeWithDots.push('...');
        }
      }
      rangeWithDots.push(page);
      previousPage = page;
    })

    /**END */

    return rangeWithDots;

  }

}
