import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ScholarshipCardComponent } from '../scholarship-card/scholarship-card.component';


@NgModule({
  declarations: [HomeComponent, ScholarshipCardComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
