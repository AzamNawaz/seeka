import { Component, OnInit } from '@angular/core';
import { Scholarship } from 'src/Models/scholarschips-Interface';
import { scholarships } from 'src/utility/scholarships';
import { CommonService } from '../services/common-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  tabs: string[] = ['Scholarship', 'Country', 'Deadline', 'Institution', 'Study Level', 'Validity']
  scholarships: Scholarship[] = [];
  activeTab: string = '';
  itemsFound: number = scholarships.length

  /**Pagination */
  itemsPerPage = 5;
  currentPage = 1;
  lastPage: number = Math.ceil(scholarships.length / this.itemsPerPage)
  maxSize = 9;
  pages: any[] = []

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {

    /**By default activate first tab */
    this.activeTab = this.tabs[0]

    /**Set pages list by deafult current page will be 1 */
    this.setPagesList(1);

  }

  /**Active Tab will be changed according to title */
  changeTab(tab: string) {

    this.activeTab = tab;

  }

  /**Open previous page in pagination */
  prevPage() {

    this.setPage(this.currentPage - 1);

  }
  /**Open next page in pagination */
  nextPage() {

    this.setPage(this.currentPage + 1);

  }

  /**Set current page */
  setPage(page: any) {

    this.currentPage = page;

    this.setPagesList(page)

  }

  /**Change pages list according to current page */
  setPagesList(page: any) {

    this.pages = this.commonService.pagination(page, this.lastPage);

    /**Filtering recipes according to current page and items per page */
    this.scholarships = scholarships.slice((this.currentPage - 1) * this.itemsPerPage, this.currentPage * this.itemsPerPage);
  }

}
